import configparser
import re
import subprocess
import time
from datetime import datetime


class Check():
    def __init__(self, bookfile, userfile):
        # Takes a book and user config files as an input
        self.bookfile = bookfile
        self.userfile = userfile
        self.bookconfig = configparser.ConfigParser()
        self.bookconfig.read(self.bookfile)
        self.userconfig = configparser.ConfigParser()
        self.userconfig.read(self.userfile)

    def check_valid_user(self, user):
        match = re.match(r"[0-9]{4}", user)
        if match:
            return user
        print("the user is not valid")

    def check_user_existance(self, user):
        if user in self.userconfig.sections():
            return user
        print("the user doesn't exist")

    def check_valid_book(self, book):
        match = re.match(r"[0-9]{10}", book)
        if match:
            return book
        print("the book is not valid")

    def check_book_existance(self, book):
        if book in self.bookconfig.sections():
            return book
        print("the book doesn't exists")

    def update_file(self, fil, data):

        with open(fil, "w") as fid:
            data.write(fid)

    def first_reservation(self, book):
        now = datetime.now()
        tim = now.strftime("%m/%d/%Y")
        old_reserve = time.strptime(tim, "%d/%m/%Y")

        for item in self.bookconfig.options(book):
            if "reserve" in item:
                reserve_time = time.strptime(self.bookconfig[book][item], "%d/%m/%Y")
                if reserve_time < old_reserve:
                    old_reserve = reserve_time
                    option = item
        return option

    def notification(self, book, user):
        print("Dear {} user, the book with {} ID is available now".format(user, book)),
        time.sleep(5)
        self.remove()

    def remove(self):
        tput = subprocess.Popen(['tput', 'cols'], stdout=subprocess.PIPE)
        cols = int(tput.communicate()[0].strip())
        print("\033[A{}\033[A".format(''.join([' ']*cols)))

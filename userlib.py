import configparser
from checks import Check
from datetime import datetime


class Users:

    def __init__(self, bookfile, userfile):
        # Takes a book and user config files as an input
        self.CHK = Check(bookfile, userfile)
        self.bookfile = bookfile
        self.userfile = userfile
        self.userconfig = configparser.ConfigParser()
        self.userconfig.read(self.userfile)
        self.bookconfig = configparser.ConfigParser()
        self.bookconfig.read(self.bookfile)
        self.CHK = Check(bookfile, userfile)

    def add_user(self):
        while True:
            userId = input("Insert user ID which contaions 4 digits: ")
            if self.CHK.check_valid_user(userId):
                break

        while True:
            name = input("insert user name and surname: ")
            if len(name.split()) >= 2:
                break
            elif len(name.split()) == 1:
                print("the name or surname is missing")
            else:
                print("name is not valid")

        if userId not in self.userconfig.sections():
            self.userconfig.add_section(userId)
            self.userconfig[userId]["name"] = name
            self.CHK.update_file(self.userfile, self.userconfig)
        else:
            print("the user already there")

    def remove_user(self):
        while True:
            userId = input("Insert user ID which contaions 4 digits: ")
            if self.CHK.check_valid_user(userId):
                break

        # Check existance of any overdue book
        # some code
        overdue_books = self.overdue_books(userId)
        if overdue_books:
            self.total_fine(userId)
            print("this user need to pay for overdue books")
        else:
            if userId in self.userconfig.sections():
                self.userconfig.remove_section(userId)
                self.CHK.update_file(self.userfile, self.userconfig)
            else:
                print("the user doesn't exist in file")

    def overdue_books(self, user):
        if self.CHK.check_valid_user(user) and \
          self.CHK.check_user_existance(user):
            checkout_bookid = []
            overdue_books = {}
            for items in self.userconfig[user]:
                if "checkout" in items:
                    checkout_bookid.append(items.split("_")[0])
            if len(checkout_bookid) == 0:
                print("user doesn't have checkout book")
            for bookid in checkout_bookid:
                try:
                    self.userconfig[user]["{}_return".format(bookid)]
                    checkout_time = datetime.strptime(self.userconfig[user]["{}_checkout".format(bookid)],"%m/%d/%Y")
                    return_time = datetime.strptime(self.userconfig[user]["{}_return".format(bookid)], "%m/%d/%Y")
                    if (return_time - checkout_time).days > 90:
                        overdue_books[bookid] = (return_time - checkout_time).days - 90
                except:
                    pass

            print("overdu book list:")
            print(overdue_books)
            return overdue_books

    def total_fine(self, user):
        total_fine = 0
        overdue_book_list = self.overdue_books(user)
        for bookid in overdue_book_list.keys():
            overdue_days = overdue_book_list[bookid]
            overdue_fine = (overdue_days/7)*5
            total_fine = total_fine + overdue_fine

        print("total fine is {}$".format(total_fine))
        return total_fine

    def book_fine(self, book, user):
        if self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book):
            overdue_book_list = self.overdue_books(user)
            if book in overdue_book_list.keys():
                overdue_days = overdue_book_list[book]
                overdue_fine = (overdue_days/7)*5
            print("book overdue fine is {}$".format(overdue_fine))

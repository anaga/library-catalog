from booklib import Books
from userlib import Users
import os


def check_file(fil):
    if os.path.isfile(fil):
        try:
            with open(fil, "w"):
                return fil
        except PermissionError as e:
            print(e)
    else:
        print("no such file or valid symlink")


bookfile = check_file("book.ini")
userfile = check_file("user.ini")

if bookfile and userfile:

    BK = Books(bookfile, userfile)

    # Return book
    # BK.return_book("0140449267", "1234")

    # Add book
    # BK.add_book()

    # Remove book
    # BK.remove_book()

    # Reserve book
    # BK.reserve_book("0140449267", "1234")

    # Get subscribers list
    # BK.get_subscribers("0140449267")

    # check available copies
    # BK.check_available_book("0140449267")

    # Checkout the book
    # BK.check_out("0140449267", "1234")

    # Get Checkout users
    # BK.get_checkout_users("0140449267")

    ##################################################
    USR = Users(bookfile, userfile)

    # add user
    # USR.add_user()

    # remove user
    # USR.remove_user()

    # Overdue Books of user
    # USR.overdue_books("1234")

    # Get fine for overdue book of the user
    # USR.book_fine("0140449264", "1234")

    # Total fine of user
    # USR.total_fine("1234")

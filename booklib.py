from datetime import datetime
import configparser
from checks import Check


class Books:

    def __init__(self, bookfile, userfile):
        # Takes a book and user config files as an input
        self.bookfile = bookfile
        self.userfile = userfile
        self.bookconfig = configparser.ConfigParser()
        self.bookconfig.read(self.bookfile)
        self.userconfig = configparser.ConfigParser()
        self.userconfig.read(self.userfile)

    def check_out(self, book, user):  # Takes book ID and user ID as an input
        # Check valid book ID, user ID and their existance in configuration files.
        if self.CHK.check_valid_user(user) and \
          self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book) and \
          self.CHK.check_user_existance(user):

            # Check if available copies are there
            if int(self.bookconfig[book]["available_copies"]) != 0:
                # check if user is not checkout the book already
                try:
                    self.userconfig[user]["{}_checkout".format(book)]
                    try:
                        self.userconfig[user]["{}_return".format(book)]
                        count = int(self.bookconfig[book]["available_copies"]) - 1
                        self.bookconfig.set(book, "available_copies", str(count))
                        self.CHK.update_file(self.bookfile, self.bookconfig)
                        print("{} is checkout {} book".format(user, book))

                        # remove return option
                        self.userconfig.remove_option(user,"{}_return".format(book))

                        # save checkout date
                        now = datetime.now()
                        self.userconfig[user]["{}_checkout".format(book)] = now.strftime("%m/%d/%Y")
                        self.CHK.update_file(self.userfile, self.userconfig)
                    except:
                        print("you checkout but not return the book")
                except:
                    count = int(self.bookconfig[book]["available_copies"]) - 1
                    self.bookconfig.set(book, "available_copies", str(count))
                    self.CHK.update_file(self.bookfile, self.bookconfig)
                    print("{} is checkout {} book".format(user, book))

                    # save checkout date
                    now = datetime.now()
                    self.userconfig[user]["{}_checkout".format(book)] = now.strftime("%m/%d/%Y")
                    self.CHK.update_file(self.userfile, self.userconfig)

                # check if user is in reserve list.
                # remove if yes
                try:
                    self.bookconfig[book]['{}_reserve_date'.format(user)]
                    print("You reserved this book")
                    print("Removed from reserve list")
                    self.bookconfig.remove_option(book, '{}_reserve_date'.format(user))
                    self.CHK.update_file(self.bookfile, self.bookconfig)
                except:
                    pass

            elif int(self.bookconfig[book]["available_copies"]) == 0:
                print("The book is not available.")
                try:
                    self.bookconfig[book]['{}_reserve_date'.format(user)]
                    print("You reserved this book already.")
                except:
                    print("You can reserve it.")

    def return_book(self, book, user):  # Takes book ID and user ID as an input
        # Check valid book ID, user ID and their existance in configuration files.
        if self.CHK.check_valid_user(user) and \
          self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book) and \
          self.CHK.check_user_existance(user):

            count = int(self.bookconfig[book]["available_copies"]) + 1
            self.bookconfig.set(book, "available_copies", str(count))
            self.CHK.update_file(self.bookfile, self.bookconfig)

            # check if someone reserve the book
            resuser = self.CHK.first_reservation(book)
            # if yes print notification
            if resuser:
                self.CHK.notification(book, resuser)

            # save return time
            now = datetime.now()
            self.userconfig[user]["{}_return".format(book)] = now.strftime("%m/%d/%Y")
            self.CHK.update_file(self.userfile, self.userconfig)

    def reserve_book(self, book, user):
        # Check valid book ID, user ID and their existance in configuration files.
        if self.CHK.check_valid_user(user) and \
          self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book) and \
          self.CHK.check_user_existance(user):

            # Check if available copies are not there
            if int(self.bookconfig[book]["available_copies"]) == 0:
                now = datetime.now()

                # check if user not in reserve list already
                if self.userconfig[user]["name"] not in self.get_subscribers(book):
                    self.bookconfig[book]['{}_reserve_date'.format(user)] = now.strftime("%m/%d/%Y")
                    self.CHK.update_file(self.bookfile, self.bookconfig)
                else:
                    print("you already reserved this book")

            else:
                print("This book is available, you can checkout it")

    def get_subscribers(self, book):
        if self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book):

            reserved_user_list = []
            for items in self.bookconfig[book]:
                if "reserve" in items:
                    user = items.split("_")[0]

                    if self.CHK.check_user_existance(user):
                        reserved_user_list.append(self.userconfig[user]["name"])
                    else:
                        print("user doesn't exist in users list")

            print("users  who reserve the book")
            print(",".join(reserved_user_list))
            return ",".join(reserved_user_list)

    def check_available_book(self, book):
        if self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book):

            if int(self.bookconfig[book]["available_copies"]) != 0:
                print("available copies of the book is {}".format(self.bookconfig[book]["available_copies"]))
            else:
                print("no any available copies")

    def get_checkout_users(self, book):
        if self.CHK.check_valid_book(book) and \
          self.CHK.check_book_existance(book):

            checkout_user_list = []
            for items in self.userconfig.sections():
                for option in self.userconfig.options(items):
                    if "{}_checkout".format(book) in option:
                        checkout_user_list.append(self.userconfig[items]["name"])
            print("users who checkout the book")
            print(",".join(checkout_user_list))
            return ",".join(checkout_user_list)

    def add_book(self):
        while True:
            bookId = input("Insert book ISBN which contaions 10 digits: ")
            if self.CHK.check_valid_book(bookId):
                break

        while True:
            pages = input("insert integer valu for the book pages: ")
            if pages.isdigit():
                break
            print("the value is not valid")

        while True:
            copies = input("insert integer value of book copies: ")
            if copies.isdigit():
                break
            print("the value is not valid")

        while True:
            count = input("insert integer value for available copies: ")
            if count.isdigit():
                break
            print("the value is not valid")

        while True:
            title = input("insert the book title: ")
            if title is not None:
                break
            print("the value is not valid")

        if bookId not in self.bookconfig.sections():
            self.bookconfig.add_section(bookId)
            self.bookconfig[bookId]["title"] = title
            self.bookconfig[bookId]["pages"] = pages
            self.bookconfig[bookId]["copies"] = copies
            self.bookconfig[bookId]["available_copies"] = count
            self.CHK.update_file(self.bookfile, self.bookconfig)
        else:
            print("The bookID is already exists")

    def remove_book(self):
        while True:
            bookId = input("Insert book ISBN which contaions 10 digits: ")
            if self.CHK.check_valid_book(bookId):
                break
        if bookId in self.bookconfig.sections():
            self.bookconfig.remove_section(bookId)
            self.CHK.update_file(self.bookfile, self.bookconfig)
        else:
            print("the book doesn't exist in file")
